import java.io.*;

public class IO_Handler 
{
	
	public static byte[] readFile()
	{
		byte[] data = null;
		FileInputStream in_stream = null;
		
		try	
		{
			in_stream = new FileInputStream("Input/original.txt");			// oeffne File 
			data = in_stream.readAllBytes();								// Lese Text aus und speichere Ihn ab
			in_stream.close();
		}
		catch (Exception e)
		{
			System.out.println("Das File konnte nicht ausgelesen werden");
			System.out.println(e.getMessage());
		}
		
		return data;
	}
	
	public static void write(byte[] data, String filename)
	{
		FileOutputStream out_stream = null;
		
		try
		{
			out_stream = new FileOutputStream("Output/" + filename);
			out_stream.write(data);
			out_stream.close();
		}
		catch (Exception e)
		{
			System.out.println("Das File konnte nicht beschrieben werden");
			System.out.println(e.getMessage());
		}
	}
}
