public class Main {

    public static void main(String[] args) 
    {
    	// Byte Arrays zum Abspeichern des jeweiligen Textes, Encrypter erstellen
    	byte[] klartext, verschluesselt, entschluesselt;
    	RSAEncrypt encrypter = new RSAEncrypt(2024);
    	
    	// Klartext einlesen, verschluesseln und in .txt File schreiben
    	klartext = IO_Handler.readFile();
    	verschluesselt = encrypter.encrypt(klartext);
    	IO_Handler.write(verschluesselt, "verschluesselt.txt");
    	
    	// Verschluesselten Text einlesen, entschluesseln und in .txt File schreiben
    	entschluesselt = encrypter.decrypt(verschluesselt);
    	IO_Handler.write(entschluesselt, "entschluesselt.txt");
    	
    	System.out.println("Fertig!");
    }
}
