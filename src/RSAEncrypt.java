import java.security.KeyPair;
import java.security.KeyPairGenerator;
import javax.crypto.Cipher;

public class RSAEncrypt {

    private KeyPair keys;
    private Cipher encryptCipher;
    private Cipher decryptCipher;


    public RSAEncrypt(int keysize)
    {
        try
        {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(keysize);
            keys = keyGen.generateKeyPair();
            encryptCipher = Cipher.getInstance("RSA");
            encryptCipher.init(Cipher.ENCRYPT_MODE, keys.getPublic());
            decryptCipher = Cipher.getInstance("RSA");
            decryptCipher.init(Cipher.DECRYPT_MODE, keys.getPrivate());
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public byte[] encrypt(byte[] klartext)
    {
        byte code[] = new byte[klartext.length];
        try
        {
            code = encryptCipher.doFinal(klartext);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return code;
    }

    public byte[] decrypt(byte[] code)
    {
        byte klartext[] = new byte[code.length];
        try
        {
            klartext = decryptCipher.doFinal(code);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

        return klartext;
    }
}
